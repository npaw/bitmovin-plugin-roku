## [6.5.6] - 2022-06-09
### Added
- `onStopVideo` method on YBPluginBitmovinPlayer to modify behaviour of the tracking when the player changes to state `error` or unloads the content

## [6.5.5] - 2022-02-03
### Added
- Timeshift events now trigger seek requests
- event close to end `communication` and `YBPluginGeneric` classes while loops

## [6.5.4] - 2021-09-20
### Added
- Lib update, customDimensions support

## [6.5.3] - 2021-09-02
### Added
- Bitrate and rendition detection

## [6.5.2] - 2021-06-17
### Removed
- `currentTime` event references
### Added
- `timeChanged` event references
- Support for +1.40.0 player version

## [6.5.1] - 2021-02-17
### Added
- Support adPause and adResume events

## [6.5.0] - 2020-11-04
### Added
- Support for bitmovin ads