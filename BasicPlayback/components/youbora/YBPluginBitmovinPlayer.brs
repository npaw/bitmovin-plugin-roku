sub init()
    YouboraLog("YBPluginBitmovinPlayer.brs - init")
end sub

sub startMonitoring()

    m.pluginName = "BitmovinPlayer"
    m.pluginVersion = "6.5.6-" + m.pluginName

    m.BitmovinFunctions = m.top.videoplayer.BitmovinFunctions
    m.BitmovinFields = m.top.videoplayer.BitmovinFields
    m.BitmovinPlayerState = m.top.videoplayer.BitmovinPlayerState

    'm.top.videoplayer.ObserveField(m.BitmovinFields.TIME_CHANGED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.ERROR, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.METADATA, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.PLAY, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.PLAYER_STATE, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.SEEK, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.SEEKED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.SOURCE_LOADED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.SOURCE_UNLOADED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.TIME_SHIFT, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.TIME_SHIFTED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.WARNING, m.port)

    m.top.videoplayer.ObserveField(m.BitmovinFields.AD_BREAK_FINISHED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.AD_BREAK_STARTED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.AD_ERROR, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.AD_INTERACTION, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.AD_QUARTILE, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.AD_FINISHED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.AD_STARTED, m.port)
    m.top.videoplayer.ObserveField(m.BitmovinFields.AD_SKIPPED, m.port)

    m.duration = invalid
    m.isLive = false
    m.config = invalid

    m.adduration = invalid
    m.adtitle = invalid
    m.adurl = invalid
    m.adid = invalid
    m.click = invalid
    m.creativeid = invalid
    m.duration = invalid
    m.position = invalid
    m.adbreakcount = invalid

    m.playingAds = false

end sub

' Player events

'sub onCurrentTime()
    '? "ON CURRENT TIME "
'end sub

'This method can be overriden to control retry scenarios, to keep them in one view
'so its important to keep it unmodified calling only 'error' to avoid side effects
sub onVideoError()
  print "ERROR: "; m.top.videoplayer.error.code.toStr() + ": " + m.top.videoplayer.error.message
  eventHandler("error", {"msg":m.top.videoplayer.error.message, "errorCode":m.top.videoplayer.error.code.toStr()})
end sub

'This method can be overriden to control retry scenarios, to keep them in one view
'so its important to keep it unmodified calling only 'stop' to avoid side effects
sub onStopVideo()
  eventHandler("stop")
end sub

sub onMetadataChanged()
    ? "ON metadata changed"
end sub

sub onPlay()
    ? "ON PLAY "
    eventHandler("resume")
end sub

sub onStateChange()
    currentState = m.top.videoplayer.playerState
    bitmovinState = m.top.videoplayer.BitmovinPlayerState
    ? "Bitmovin state:"; currentState
    if currentState = bitmovinState.PLAYING then
      if m.playingAds then 
        eventHandler("adResume")
      else
        eventHandler("resume")
      end if
        eventHandler("buffered")
        if m.duration = invalid then m.duration = m.top.videoplayer.callFunc(m.BitmovinFunctions.GET_DURATION)
        if m.isLive = invalid then m.isLive = m.top.videoplayer.callFunc(m.BitmovinFunctions.IS_LIVE)
        ? "Duration: "
        eventHandler("join")
    else if currentState = bitmovinState.STALLING then
        if m.viewManager.isSeeking = false and m.viewManager.isPaused = false then
          eventHandler("buffering")
        end if
    else if currentState = bitmovinState.PAUSED then
      if m.viewManager.isSeeking = false then
        if m.playingAds then 
          eventHandler("adPause")
        else
          eventHandler("pause")
        end if
      end if
    else if currentState = bitmovinState.FINISHED then
        eventHandler("stop")
    else if currentState = bitmovinState.ERROR then
        onStopVideo()
    else if currentState = bitmovinState.NONE then
        'eventHandler("stop")
    else if currentState = bitmovinState.SETUP then
        'eventHandler("play")
    else if currentState = bitmovinState.READY then
        eventHandler("play")
        'if m.duration = invalid then m.duration = m.top.videoplayer.callFunc(m.BitmovinFunctions.GET_DURATION)
        'if m.isLive = invalid then m.isLive = m.top.videoplayer.callFunc(m.BitmovinFunctions.IS_LIVE)
        eventHandler("join")
    end if
end sub

sub onSeek()
  print "SEEKING"
  eventHandler("resume")
  eventHandler("seeking")
end sub

sub onSeeked()
  print "SEEKED: "; m.top.videoplayer.seeked
  eventHandler("seeked")
end sub

sub onSourceLoaded()
  ? "SOURCE LOADED: "
  m.config = m.top.videoplayer.callFunc(m.BitmovinFunctions.GET_CONFIG)
  'm.isLive = m.top.videoplayer.isLive
  'm.duration = m.top.videoplayer.callFunc(m.BitmovinFunctions.GET_DURATION)
  eventHandler("play")
end sub

sub onSourceUnloaded()
  ? "SOURCE UNLOADED: "
  onStopVideo()
  m.duration = invalid
  m.isLive = invalid
  m.config = invalid
end sub

sub onTimeShift()
  ? "TIME SHIFT: "
  eventHandler("seeking")
end sub

sub onTimeShifted()
  ? "TIME SHIFTED: "
  eventHandler("seeked")
end sub

sub onWarning()
  ? "WARNING: "
end sub

sub processMessage(msg, port)
  mt = type(msg)
    if mt = "roSGNodeEvent"
      'if msg.getField() = m.BitmovinFields.TIME_CHANGED
            'onCurrentTime()
        'else 
        if msg.getField() = m.BitmovinFields.ERROR
            onVideoError()
        else if msg.getField() = m.BitmovinFields.METADATA
            onMetadataChanged()
        else if msg.getField() = m.BitmovinFields.PLAY
            onPlay()
        else if msg.getField() = m.BitmovinFields.PLAYER_STATE
            onStateChange()
        else if msg.getField() = m.BitmovinFields.SEEK
            onSeek()
        else if msg.getField() = m.BitmovinFields.SEEKED
            onSeeked()
        else if msg.getField() = m.BitmovinFields.SOURCE_LOADED
            onSourceLoaded()
        else if msg.getField() = m.BitmovinFields.SOURCE_UNLOADED
            onSourceUnloaded()
        else if msg.getField() = m.BitmovinFields.TIME_SHIFT
            onTimeShift()
        else if msg.getField() = m.BitmovinFields.TIME_SHIFTED
            onTimeShifted()
        else if msg.getField() = m.BitmovinFields.WARNING
            onWarning()
        else if msg.getField() = m.BitmovinFields.AD_BREAK_FINISHED
            eventHandler("adBreakStop")
        else if msg.getField() = m.BitmovinFields.AD_BREAK_STARTED
            data = msg.getData()
            if data <> invalid 

              if data.adbreak <> invalid and data.adbreak.ads <> invalid and type(data.adbreak.ads) = "roArray" 
                m.adbreakcount = data.adbreak.ads.Count()
              end if

              if data.scheduletime = 0
                m.position = "pre"
              else if data.scheduletime = getMediaDuration()
                m.position = "post"
              else
                m.position = "mid"
              end if

            end if
            eventHandler("pause")
            eventHandler("adBreakStart")
        else if msg.getField() = m.BitmovinFields.AD_ERROR
            'data = msg.getData()
            'eventHandler("adError", {"msg": data.message, "errorCode": data.code})
        else if msg.getField() = m.BitmovinFields.AD_INTERACTION
            eventHandler("adClick", {"url": m.click})
        else if msg.getField() = m.BitmovinFields.AD_QUARTILE
            eventHandler("adQuartile")
        else if msg.getField() = m.BitmovinFields.AD_FINISHED
            m.playingAds = false
            eventHandler("adStop")
        else if msg.getField() = m.BitmovinFields.AD_STARTED
            m.playingAds = true
            data = msg.getData()
            if data <> invalid and data.ad <> invalid and data.ad.data <> invalid
              data2 = data.ad.data
              m.adduration = data2.duration
              m.adtitle = data2.adtitle
              m.adurl = data2.adserver
              m.click = data2.clickthrough
              m.creativeid = data2.creativeid
            end if
            eventHandler("adStart")
            eventHandler("adJoin")
        else if msg.getField() = m.BitmovinFields.AD_SKIPPED
            eventHandler("adStop", {"skipped": true})
        endif
    endif
end sub

' Getters

function getResource()
  if m.config <> invalid then
    if m.config.source <> invalid then
      m.source = m.config.source
      if m.source.hls <> invalid then
        return m.source.hls
      else if m.source.dash <> invalid then
        return m.source.dash
      else if m.source.smooth <> invalid then
        return m.source.smooth
      else if m.source.progressive <> invalid then
        return m.source.progressive
      end if
    end if
  end if

  return invalid
end function

function getMediaDuration()
    return m.duration
end function

function getPlayhead()
    return m.top.videoplayer.currentTime
end function

function getTitle()
  if m.config <> invalid then
    if m.config.source <> invalid then
      if m.config.source.title <> invalid then
        return m.config.source.title
      end if
    end if
  end if
end function

function getIsLive()
    return m.isLive
end function

function getThroughput()
    return 0
end function

function getBitrate()
  m.qualitychange = m.top.videoplayer.videoDownloadQualityChanged
  if m.qualitychange <> invalid AND m.qualitychange.targetquality <> invalid
    br = m.qualitychange.targetquality.bitrate
  else
    br = -1
  end if
  return br
end function

function getRendition()
  m.qualitychange = m.top.videoplayer.videoDownloadQualityChanged
  if m.qualitychange <> invalid AND m.qualitychange.targetquality <> invalid
    rendition = m.qualitychange.targetquality.bitrate
    if rendition < 1000
      rendition = rendition.ToStr() + "bps"
    else if rendition < 1000000
      rendition = (rendition / 1000).ToStr() + "Kbps"
    else
      rendAux = rendition / 1000000.0 'Divide by mega
      rendAux = Cint(rendAux * 100) / 100.0
      rendition = rendAux.ToStr() + "Mbps"
    end if
    width = m.qualitychange.targetquality.width
    height = m.qualitychange.targetquality.height
    if width <> invalid AND height <> invalid AND width <> 0 AND height <> 0
      rendition = width.ToStr() + "x" + height.ToStr() + "@" + rendition
    endif
  else
    rendition = invalid
  end if
  return rendition
end function

function getPlayerVersion()
    if m.config <> invalid then
      if m.config.version <> invalid then
        return "Bitmovin-" + m.config.version
      end if
    end if
    return "Bitmovin"
end function

function getAdDuration()
  return m.adduration
end function

function getAdTitle()
  return m.adtitle
end function

function getAdResource()
  return m.adurl
end function

function getAdPosition()
  return m.position
end function

function getAdCreativeId()
  return m.creativeid
end function

function getAdCount()
  return m.adbreakcount
end function